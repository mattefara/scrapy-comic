# Scrapy Comic
This is a project supporting the android app [Comic Reader](https://gitlab.com/mattefara/comic-reader-app) that scrapes comics from different providers

# Usage
```shell
# export FIRESTORE_EMULATOR_HOST="localhost:8080" # Using Firebase Emulator
cd scraper
scrapy crawl reaper 
scrapy crawl asura    
```