# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://docs.scrapy.org/en/latest/topics/item-pipeline.html
import json
import uuid
from datetime import datetime
from random import randint

from firebase_admin import credentials, firestore, initialize_app
from scrapy.exceptions import DropItem

from .items import ExtractedItem, ComicItem, ChapterItem, ProviderEnum, ProviderItem
from .spiders.ComicSpider import AsuraSpider, ReaperSpider


class ComicItemParserPipeline:
    def process_item(self, item: ExtractedItem, spider):
        provider = None
        if isinstance(spider, AsuraSpider):
            provider = 'asura_scans'
        if isinstance(spider, ReaperSpider):
            provider = 'reaper_scans'

        if provider is None:
            raise DropItem()

        chapters = [
            ChapterItem(
                chapter_id=str(uuid.uuid1()),
                chapter_link=chapter.chapter_url,
                chapter_name=chapter.chapter_name,
                chapter_number=len(item.chapters) - i - 1
            ) for i, chapter in enumerate(item.chapters)
        ]

        return ComicItem(
            title=item.comic.title,
            title_lower=item.comic.title.lower(),
            cover=item.comic.image_url,
            provider=provider,
            last_chapter=chapters[0],
            chapters=chapters,
            updatedAt=int(datetime.now().timestamp()) - randint(0, 60 * 60 * 24 * 30)  # One month
        )


class FirestoreLoaderPipeline:
    def open_spider(self, spider):
        cred = credentials.Certificate("./comic-reader.json")
        app = initialize_app(cred)
        self.comic_ref = firestore.client(app).collection("comics")
        self.provider_ref = firestore.client(app).collection("provider")

    def close_spider(self, spider):
        pass

    def process_item(self, item: ComicItem, spider):
        key = str(uuid.uuid4())

        if item.provider == ProviderEnum.asura:
            provider_info = {
                "provider_image_url": "https://www.asurascans.com/wp-content/uploads/2021/03/Group_1.png",
                "provider_regex": r"/[a-zA-Z\d-].*/"
            }
        elif item.provider == ProviderEnum.reaper:
            provider_info = {
                "provider_image_url": "https://reaperscans.com/images/logo-reaper-2.png",
                "provider_regex": r"/comics/[a-zA-Z\d-].*/chapters/[a-zA-Z\d-].*"
            }
        else:
            raise DropItem(f"No provider with {item.provider}, dropping it.")

        self.comic_ref.document(key).set(json.loads(item.to_json()))
        self.provider_ref.document(item.provider).set(json.loads(ProviderItem(**{
            "alias": " ".join(name.capitalize() for name in item.provider.split("_")),
            **provider_info
        }).to_json()))
        return item
