import scrapy
from selectorlib.selectorlib import Extractor
from ..items import ExtractedItem, ExtractedComic, ExtractedChapter


class ComicScraper(scrapy.Spider):

    def get_comic_extractor(self, html: str):
        path = f"extractors/{self.name}_extractor.yml"
        return Extractor.from_yaml_file(path).extract(html)

    def get_chapter_extractor(self, html: str):
        path = f"extractors/{self.name}_chapter_extractor.yml"
        return Extractor.from_yaml_file(path).extract(html)

    def parse(self, response, **kwargs):
        html = response.body.decode("utf8")
        extracted = self.get_comic_extractor(html)
        for update in extracted['updates']:
            comic_url = update['comic_url']
            yield scrapy.Request(comic_url, callback=self.parse_comic, meta={"comic": update})

    def parse_comic(self, response, **kwargs):
        html = response.body.decode("utf8")
        comic = ExtractedComic(**response.meta["comic"])

        chapters = [ExtractedChapter(**chapter) for chapter in self.get_chapter_extractor(html)["chapters"]]

        yield ExtractedItem(comic=comic, chapters=chapters)


class AsuraSpider(ComicScraper):
    name = "asura"
    start_urls = ["https://www.asurascans.com/"]


class ReaperSpider(ComicScraper):
    name = "reaper"
    start_urls = ["https://reaperscans.com/latest/comics"]