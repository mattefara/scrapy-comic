# Define here the models for your scraped items
#
# See documentation in:
# https://docs.scrapy.org/en/latest/topics/items.html

from dataclasses import dataclass
from enum import Enum

from dataclass_wizard import JSONWizard


class ProviderEnum(str, Enum):
    asura = "asura_scans"
    reaper = "reaper_scans"


@dataclass
class ExtractedChapter:
    chapter_name: str
    chapter_url: str


@dataclass
class ExtractedComic:
    title: str
    last_chapter_url: str
    last_chapter_name: str
    image_url: str
    comic_url: str


@dataclass
class ExtractedItem:
    comic: ExtractedComic
    chapters: list[ExtractedChapter]


@dataclass
class ChapterItem(JSONWizard):
    chapter_id: str
    chapter_link: str
    chapter_name: str
    chapter_number: int


@dataclass
class ComicItem(JSONWizard):
    title: str
    title_lower: str
    cover: str
    provider: str
    last_chapter: ChapterItem
    chapters: list[ChapterItem]
    updatedAt: int


@dataclass
class ProviderItem(JSONWizard):
    alias: str
    provider_image_url: str
    provider_regex: str
